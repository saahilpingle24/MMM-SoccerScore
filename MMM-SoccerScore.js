Module.register("MMM-SoccerScore", {
	defaults: {
		updateInterval: 45 * 60 * 1000,
		retryDelay: 5000,
		max: 16,
		league: ['epl']
	},

	requiresVersion: "2.1.0", // Required version of MagicMirror

	start: function() {
		var self = this;
		var dataRequest = null;
		var dataNotification = null;

		this.loaded = false;

		// Schedule update timer.
		this.getData();
		setInterval(function() {
			self.updateDom();
		}, this.config.updateInterval);
	},

	getData: function() {
		var self 			= this;
		var retry 			= true;

		this.leagueId 		= this.config.league[0];

		// upstream api is refreshed every 30 minutes
		var urlApi 		= "https://protected-fortress-73539.herokuapp.com/v1/" + this.leagueId + "/scoreline";

		var dataRequest = new XMLHttpRequest();

		dataRequest.open("GET", urlApi);

		dataRequest.onreadystatechange = function() {
			if (this.readyState === 4) {
				if (this.status === 200) {
					self.processData(JSON.parse(this.response));
				} else if (this.status === 401) {
					self.updateDom(self.config.animationSpeed);
					Log.error(self.name, this.status);
					retry = false;
				} else {
					Log.error(self.name, "Could not load data.");
				}
				if (retry) {
					self.scheduleUpdate((self.loaded) ? -1 : self.config.retryDelay);
				}
			}
		};

		dataRequest.send();
	},

	scheduleUpdate: function(delay) {
		var nextLoad = this.config.updateInterval;
		if (typeof delay !== "undefined" && delay >= 0) {
			nextLoad = delay;
		}
		nextLoad = nextLoad ;
		var self = this;
		setTimeout(function() {
			self.getData();
		}, nextLoad);
	},

	getDom: function() {
		var self = this;

		var wrapper = document.createElement("div");

		var list = document.createElement("ul");

		if (this.dataRequest) {
			var wrapperDataRequest 	= document.createElement("div");
			var labelDataRequest 	= document.createElement("label");
			var dataSize 			= (this.dataRequest.length) > this.config.max ? this.config.max : this.dataRequest.length;

			for (i=0; i < dataSize; i++) {
				var li=document.createElement('li');

				li.innerHTML = this.dataRequest[i];
				list.appendChild(li);
			}

			wrapperDataRequest.appendChild(list);
			labelDataRequest.innerHTML = this.translate(this.leagueId);


			wrapper.appendChild(labelDataRequest);
			wrapper.appendChild(wrapperDataRequest);
		}

		return wrapper;
	},

	getScripts: function() {
		return [];
	},

	getStyles: function () {
		return [
			"MMM-SoccerScore.css",
		];
	},

	getTranslations: function() {
		return {
			en: "translations/en.json",
			es: "translations/es.json",
		};
	},

	processData: function(data) {
		var self = this;
		this.dataRequest = data;
		if (this.loaded === false) { self.updateDom(self.config.animationSpeed) ; }
		this.loaded = true;

		this.sendSocketNotification("MMM-SoccerScore", data);
	},

	socketNotificationReceived: function (notification, payload) {
		if(notification === "MMM-SoccerScore") {
			this.dataNotification = payload;
			this.updateDom();
		}
	}
});
