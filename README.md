# MMM-SoccerScore

This is a module for the [MagicMirror²](https://github.com/MichMich/MagicMirror/).

It displays the recent results for you favorite soccer leagues.

## Using the module

To use this module, add the following configuration block to the modules array in the `config/config.js` file:
```js
var config = {
    modules: [
        {
        	position: "top_right",
            module: 'MMM-SoccerScore',
            header: "Soccer scoreline",
            config: {
                // See below for configurable options
            }
        }
    ]
}
```

## Configuration options

| Option           | Description
|----------------- |-----------
| `max`        | *Optional* The maximum number of results you wish to display.<br><br>**Type:** `int` <br> **Default:** 5 (capped at 16)
| `league`        | *Optional* The league for which you would like to view the scoreline. <br><br>**Type:** `array` <br>**Supported values:** One of - epl, laliga, bundesliga, seriea, ligue1, ucl, carabao, fa <br>**Default:** ["epl"]

## Screenshot
![MMM-SoccerScore](https://i.imgur.com/g5A5ZvW.png "MMM-SoccerScore")

## To-Do

Supporting multiple leagues and max results per league.

*Thanks to [Roramirez](https://github.com/roramirez/MagicMirror-Module-Template) for the module template*
